# Handball Rules

[[_TOC_]]

## Playing Area

### Surface

A suitable playing area shall be a hard, flat surface.

### Dimensions

The playing area shall be a quadrilateral shape, such as a square or rectangle, and shall be divided into at least 2 equal partitions. Each partition is referred to as a "square", regardless of whether it's actually a square or a rectangle.

The playing area can incorporate any number of contiguous squares, provided one dimension of the playing area is limited to 2 squares. For example, popular playing surfaces are 2x1 (two square), 2x2 (four square), and 2x3 (six square).

### Boundaries

The playing area's boundaries shall be clearly marked, as well as each square's boundaries.

### Square heirarchy

The lowest square in the heirarchy is called "Dunce", and the highest square in the heirarchy is "Ace". "Dunce" and "Ace" squares shall face each other, and are the starting squares on the playing area's two-sided dimension. Each square further anti-clockwise from Dunce progressively increases in heirarchy.

### Player Arrangement

Each individual square can accommodate one player only. Players can transit to an adjacent square momentarily only when attempting a legitimate play of the ball that has landed in their own square, however they must also abide by interference rules.

## The Ball

The ball shall be of similar size to a tennis ball. The ball's material and construction shall allow for moderate bounciness when dropped at arm's length.

## Consensus

"Consensus" is reached when either the majority of players, or the referree, determine the outcome of a match. Consensus must be reached to determine if any serving or playing faults have been committed.

## Server and Receiver
The player in the Ace square shall serve the ball to any player in the playing area of their choosing.

The receiving player shall make a play at the ball and bounce the ball towards any other player.

## Valid Serve

A serve's purpose is to initiate the match. The intention of a serve is not to challenge the receiving player. The serving player must serve the ball according to the following rules:

- A serve must not commence until all players are inside their respective squares
- A serve shall be of moderate height and speed, directed towards the middle of the receiving player's square
- A serve shall bounce only once in the server's square

## Serve Challenge

The receiving player can challenge the serving player's serve, based on the definition of a "valid serve". The challenge is successful if the players reach consensus in favour of the receiving player. This consitutes a serve fault, and the serving player shall attempt serving the ball again.

## Serve Fault Elimination

The serving player can be eliminated if they have commmitted two serving faults in a row.

## Ball in Play

The ball is in play from the moment the serving playing make a valid serve. The ball remains in play until:

- A player is eliminated
- A serve fault is called
- A playing fault is called
- "Lines" is called
- An outside object interferes with play
- A player interferes with another player


## Ball Touches a Permanent Fixture

If the ball in play touches a permanent fixture after it has bounced within the receiving player's square, the receiving player is eliminated. If the ball in play touches a permanent fixture before it has bounced within the receiving player's square, the serving player is eliminated.

## Playing the Ball

Playing the ball is when a receiving player receives the ball and attempts to hit it to another player's square. The play must involve striking the ball with a limb. A valid play of the ball shall avoid all playing faults.

## Playing Faults

A player who commits any of the following playing faults during play is eliminated from the match:

- "Full": When the ball does not bounce in their own square first
- "Full Play": When a receiving player intentionally continues to play the ball after a "Full" faul has occurred
- "Out": When the ball lands outside the court and hasn't first bounced in their own square, and subsequently in the receiving player's square
- "Doubles": When the ball bounces twice in a player's square
- "Double Touch": When the ball is touched twice in a row by the same person
- "Grabs": When the ball is held in a player's hands or between two or more fingers. A player can cup their hands slightly while striking the ball, but the hand can't make prolonged contact with the ball.
- "Interference": When a player interferes with another player while they are playing the ball
- "Rolls": When the ball rolls instead of bounces after being played at

Playing faults are called either by players or referrees. The playing fault must be called during the same match, and must be called as soon as the fault is observed.

## Player Interference

A player is eliminated when consensus is in favour of player interference.

- If a player transits into another player's square and interferes with another player's ability to play the ball, the interfering player is eliminated
- If a player transits to another player's square at any time, it must be during a legitimate play of the ball. If the player does not play the ball while in another player's square, the interfering player is eliminated

## Match Replay

A match is replayed if consensus is reached regarding the following conditions:

- "Lines"
- Outside interference

## Ball Touches a Line

"Lines" occurs when the ball lands on any boundary line of the playing area. "Lines" shall be called by any player or participant. The outcome of the match depends on whether "Lines" had occurred based on consensus.

- If consensus is in favour of "Lines", the match is replayed and no player is eliminated.
- If consensus is not in favour of "Lines", a player must be elininated.

## Player Elimination

Player elimination occurs when consensus is reached that a player should be eliminated. This is also referred to as being "out".

The eliminated player exits the playing area and joins the end of line of players waiting to join the game.